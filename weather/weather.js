const request = require('request');

var getWeather = (lat, lng, callback) => {

  request({
    url: `https://api.forecast.io/forecast/6eaee3391812ddbc66f0cfd6571cfc8d/${lat},${lng}`,
    json: true
  }, (error, response, body) => {
    if(!error && response.statusCode === 200) {
      callback(undefined, {
        temperature: body.currently.temperature,
        apparentTemperature: body.currently.apparentTemperature
      });
    } else {
      callback('Unable to fetch weather');
    }
  });
}

module.exports.getWeather = getWeather;
